# License Hint

Copyright © 2022 Helmholtz Centre for Environmental Research (UFZ)  
Copyright © 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

This work is licensed under the [CC-BY-4.0](LICENSES/CC-BY-4.0.txt) license if not stated otherwise.

Please see the individual files for more accurate information.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).
